﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour {

	public GameObject _directionObject;
	public bool _isDefeated;

	// Use this for initialization
	void Start () {
		transform.LookAt (_directionObject.transform);
	}

	// Update is called once per frame
	void Update () {
		if (!_isDefeated) {		
			transform.Translate (transform.forward * 0.01f, Space.World);
		} else {		
			Invoke("Defeated", 2.0f);
		}


	}

	void Defeated()
	{
		Destroy (gameObject);
	}

}
