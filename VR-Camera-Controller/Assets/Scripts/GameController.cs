﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public GameObject _zombie;

	float _timeOut;
	float _timeElapsed;

	bool _isPaused;

	// Use this for initialization
	void Start () {
		_isPaused = true;
	}

	// Update is called once per frame
	void Update () {

		if ((Input.GetKeyDown (KeyCode.Space)) ||
			(GvrController.ClickButtonUp)) {
			_isPaused = !_isPaused;
		}

		if (!_isPaused) {
			_timeElapsed += Time.deltaTime;

			if (_timeElapsed >= 5.0f) {
				Instantiate (_zombie, new Vector3 (Random.Range (-10.0f, 10.0f), 0.0f, Random.Range (-10.0f, 10.0f)), Quaternion.identity);
				_timeElapsed = 0.0f;
			}
		}
	}
}
